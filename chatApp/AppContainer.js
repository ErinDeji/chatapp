import React from "react";
import { Button, StyleSheet, Text, View, TextInput } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import FriendListScreen from "./screens/FriendListScreen";
import ChatScreen from "./screens/ChatScreen";
import JoinScreen from "./screens/JoinScreen";

const Stack = createStackNavigator();

export default function AppContainer() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Join">
        <>
          <Stack.Screen name="Home" component={FriendListScreen} />
          <Stack.Screen name="Chat" component={ChatScreen} />
          <Stack.Screen name="Join" component={JoinScreen} />
        </>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
