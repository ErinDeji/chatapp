const messageHandler = require("./handlers/message.handlers");
const io = require("socket.io")();

let currentUserId = 2;
const users = {};

const createUserAvatarUrl = () => {
  const rand1 = Math.round(Math.random() * 200 + 100);
  const rand2 = Math.round(Math.random() * 200 + 100);
  return `http://placeimg.com/${rand1}/${rand2}/any`;
};

io.on("connection", (socket) => {
  console.log("A user Connected");
  // console.log(socket.id);
  users[socket.id] = { userId: currentUserId++ };
  socket.on("join", (username) => {
    users[socket.id].username = username;
    users[socket.id].avatar = createUserAvatarUrl();
  });
  messageHandler.handleMessage(socket, users);
});
// socket.on("action", (action) => {
//   switch (action.type) {
//     case "server/hello":
//       console.log("Got hello event", action.data);
//       socket.emit("action", { type: "message", data: "Good day!" });
//   }
// });

io.listen(3000);
