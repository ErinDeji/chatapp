let currentMessageId = 1;

const createMessage = (user, messageText) => {
  return {
    _id: currentMessageId++,
    text: messageText,
    createdAt: new Date(Date.now()),
    user: {
      _id: user.userId,
      name: user.username,
      avatar: user.avatar,
    },
  };
};

const handleMessage = (socket, users) => {
  socket.on("message", (messageText) => {
    console.log(messageText);
    const userId = users[socket.id];
    const message = createMessage(userId, messageText);
    console.log(message);

    socket.broadcast.emit("message", message);
  });
};
module.exports = { handleMessage };
